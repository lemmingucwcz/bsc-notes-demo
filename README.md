# BSC Demo - Michal Kára

## Spuštění

- `yarn` - natažení závislostí

- `yarn start` - spustí aplikaci v development módu na portu 9000 (chvíli trvá, než se to nastartuje a zkompiluje)

- případně `yarn build` - sestaví produkční build a vypíše instrukce pro jeho spuštění

## Poznámky

### Druhý demo projekt

Mám ještě jeden projekt, který by vás mohl zajímat, dělal jsem jej pro výběrové řízení do Alzy.
Možno se podívat / naklonovat zde: [Demo pro Alzu](https://gitlab.com/lemmingucwcz/alza-demo).

Projekt je postavený na Next.JS, na který jsem zvyklejší - v demu pro vás jsem Next.JS použít nemohl kvůli požadavku na `connected-react-router`.
V tom Alza projektu je udělaná i dockerizace a CI/CD, které po pushnutí do specifické branche automaticky vyrobí docker image, pošle ho na Dockerhub
a otamtud se přes webhook automaticky deployoval do Azure cloudu.
Jen server, kde to ve výsledku běželo, je už terminovaný.

Je tam i Storybook pro vývoj komponent, náznak logování chyb na server atp.
Zase tam není Redux, ale jen předávání přes React context.

### Práce s daty

Přišlo mi škoda, aby poznámky nešly s mock api editovat, tak jsem aplikaci postavil tak, že redux state používám jako cache
dat (podobně jako když člověk pracuje s GraphQL) a operace úpravy a smazání se
stínují i tam a jejich výsledek je tak vidět v aplikaci. Operaci vytvoření jsem nakonec udělal tak, že ukládá
co přijde ze serveru (tedy je vždy vytvořena / obnovena stejná poznámka s id 3). Původně byla operace vytvoření také lokálně simulovaná, proto
mám ve state "next id". Aktualizace / smazání se posílají na server jak mají, jen se místo vrácené hodnoty uloží "správný" výsledek.

Pokud jde člověk na detail poznámky, tak se ukáže z lokální cache, ale zároveň se spustí její aktualizace z API.
Protože API vrací vždy stejnou poznámku, tak se v takovém případě ve výsledku v redux state vždy obnoví poznámka s id 2. 

Pokud chcete vyzkoušet handlování chyb, vytvořte / uložte poznámku s textem `throw`.

### Testy

V zadání bylo "alespoň jeden test", tedy jsem udělal testy pro reducer, kde je nějaká zajímavá business logika
a pro mapper, kde je to také dost užitečné. Dále jsem otestoval ságy pro nahrávání dat jako ukázku testování ság
a pak ještě udělal pár testů pro React komponentu (detail poznámky), které ukazují
test React komponenty včetně testu asynchronních akcí,

### redux-saga vs. redux-thunk

Protože byl nice-to-have požadavek na `redux-saga`, tak jsem ji do projektu implementoval. Nicméně u změnových operací jsem chtěl, aby UI
čekalo na výsledek operace a to by se ságami bylo složitější. Muselo by se to řešit buď přenášením stavu přes redux state, předáváním callbacku sáze,
nebo voláním ságy ne přes dispatch, ale přímo přes `sagaMiddleware.run()`, které se dá převést na promise.

S použitím thunk akcí je tohle jednodušší (dispatch vrací výsledek funkce - promise), tak jsem v projektu použil i je,
čímž tam mám i ukázku téhle technologie.
Nicméně v reálném projektu bych tyto dva přístupy asi nemíchal.    



