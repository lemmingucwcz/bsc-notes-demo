import HttpService from '../services/HttpService'
import { NoteMo } from '../../model/NoteMo'
import NoteMapper from '../mappers/NoteMapper'

const NotesAdapter = {
    list: async (): Promise<NoteMo[]> => {
        const data = await (await HttpService.get('/notes')).json()

        return NoteMapper.mapNotes(data)
    },

    get: async (noteId: number): Promise<NoteMo> => {
        const data = await (await HttpService.get(`/notes/${noteId}`)).json()

        const mapped = NoteMapper.mapNote(data)
        if (!mapped) {
            throw new Error('Invalid note from GET')
        }
        return mapped
    },

    create: async (title: string): Promise<NoteMo> => {
        const data = await (await HttpService.post('/notes', { title })).json()

        const mapped = NoteMapper.mapNote(data)
        if (!mapped) {
            throw new Error('Invalid note from POST')
        }
        return mapped
    },

    update: async (note: NoteMo): Promise<NoteMo> => {
        const data = await (await HttpService.put(`/notes/${note.id}`, { title: note.title })).json()

        const mapped = NoteMapper.mapNote(data)
        if (!mapped) {
            throw new Error('Invalid note from PUT')
        }
        return mapped
    },

    delete: async (noteId: number): Promise<void> => {
        await HttpService.delete(`/notes/${noteId}`)
    },
}

export default NotesAdapter
