import { NoteAo } from '../model/NoteAo'
import { NoteMo } from '../../model/NoteMo'

const NoteMapper = {
    /**
     * Map note from API to application model. Needed because we can't rely what's coming from the API.
     *
     * @param note Note from API
     *
     * @return Note when it is valid note, undefined otherwise
     */
    mapNote: (note: NoteAo): NoteMo | undefined => {
        const { id, title } = note
        if (typeof id === 'number' && id > 0 && typeof title === 'string') {
            // Validated OK, can convert
            return { id, title } as NoteMo
        }

        // eslint-disable-next-line no-console
        console.warn(`Got invalid note object from API`, note)
        return undefined
    },

    /**
     * Map array of notes. Only returns valid notes.
     *
     * @param notes API notes
     *
     * @return Array of valid notes
     */
    mapNotes: (notes: NoteAo[]): NoteMo[] =>
        // Map and filter failed items
        notes.map(NoteMapper.mapNote).filter((note) => note) as NoteMo[],
}

export default NoteMapper
