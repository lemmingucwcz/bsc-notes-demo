import NoteMapper from '../NoteMapper'

/* eslint-disable no-console */
/* eslint-disable @typescript-eslint/no-explicit-any */

describe('NoteMapper', () => {
    // Replace console.warn with mock to not produce warnings to console during test & to check it is called
    let oldWarn: any
    beforeAll(() => {
        oldWarn = console.warn
        console.warn = jest.fn()
    })

    afterAll(() => {
        console.warn = oldWarn
    })

    beforeEach(jest.clearAllMocks)

    describe('mapNote()', () => {
        it('should map valid note', () => {
            const mockNote = { id: 1, title: 'Test' }
            expect(NoteMapper.mapNote(mockNote)).toEqual(mockNote)
            expect(console.warn).not.toHaveBeenCalled()
        })

        it('should reject note with string id', () => {
            expect(NoteMapper.mapNote({ id: '1', title: 'test' } as any)).toBeUndefined()
            expect(console.warn).toHaveBeenCalled()
        })

        it('should reject note without title', () => {
            expect(NoteMapper.mapNote({ id: 1 })).toBeUndefined()
            expect(console.warn).toHaveBeenCalled()
        })

        it('should reject note with zero id', () => {
            const note = { id: 0, title: 'Test' }
            expect(NoteMapper.mapNote(note)).toBeUndefined()
            expect(console.warn).toHaveBeenCalled()
        })
    })

    describe('mapNotes', () => {
        it('should pass valid notes, reject invalid', () => {
            const mockOK1 = { id: 1, title: 'OK 1' }
            const mockOK2 = { id: 2, title: 'OK 2' }
            const res = NoteMapper.mapNotes([mockOK1, mockOK2, { id: -1, title: 'KO 1' }])

            expect(res).toHaveLength(2)
            expect(res[0]).toEqual(mockOK1)
            expect(res[1]).toEqual(mockOK2)
        })
    })
})
