/**
 * Note object on API - can't rely on its shape yet
 */
export interface NoteAo {
    readonly id?: number

    readonly title?: string
}
