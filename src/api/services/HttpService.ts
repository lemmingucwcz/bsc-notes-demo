const { API_BASE } = process.env

/**
 * HTTP calling utility
 *
 * @param endpoint Endpoint to call
 * @param init URL parameters (see fetch docs)
 * @param apiBase Which API to use (LOCAL_API_BASE / undefined (DEFAULT_API_BASE))
 */
const callHttp = async (endpoint: string, init: RequestInit) => {
    const completeUrl = `${API_BASE}/${endpoint}`
    const result = await fetch(completeUrl, init).catch((error) => {
        const message = `${init.method} to ${completeUrl} failed`

        // eslint-disable-next-line no-console
        console.error(message, error)
    })

    if (!result) {
        throw new Error('No result from fetch')
    }

    return result
}

/**
 * HTTP requests service
 */
const HttpService = {
    /**
     * Execute HTTP post
     *
     * @param endpoint Endpoint to post to (relative to API base, without leading slash)
     * @param body Request body. Either direct string, or object that will be JSON.stringified
     */
    post: (endpoint: string, body: object | string) => {
        const headers = new Headers()
        headers.append('Content-Type', 'application/json')

        return callHttp(endpoint, {
            headers,
            body: typeof body === 'string' ? body : JSON.stringify(body),
            method: 'POST',
        })
    },

    /**
     * Execute HTTP put
     *
     * @param endpoint Endpoint to put to (relative to API base, without leading slash)
     * @param body Request body. Either direct string, or object that will be JSON.stringified
     */
    put: (endpoint: string, body: object | string) => {
        const headers = new Headers()
        headers.append('Content-Type', 'application/json')

        return callHttp(endpoint, {
            headers,
            body: typeof body === 'string' ? body : JSON.stringify(body),
            method: 'PUT',
        })
    },

    /**
     * Execute HTTP delete
     *
     * @param endpoint Endpoint to put to (relative to API base, without leading slash)
     */
    delete: (endpoint: string) => {
        return callHttp(endpoint, { method: 'DELETE' })
    },

    /**
     * Execute HTTP get
     *
     * @param endpoint Endpoint to get from (relative to API base, without leading slash)
     * @param apiBase API base to use (default is proxy to REST server)
     */
    get: (endpoint: string) => callHttp(endpoint, { method: 'GET' }),
}

export default HttpService
