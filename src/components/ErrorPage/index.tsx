import React from 'react'
import { useHistory } from 'react-router'
import { createStyles, makeStyles } from '@material-ui/core/styles'
import { FormattedMessage } from 'react-intl'
import { Button, Paper, Typography } from '@material-ui/core'
import PageFrame from '../common/PageFrame'
import { ROUTE_LIST } from '../../utils/routeUtils'

const useStyles = makeStyles(() =>
    createStyles({
        paper: {
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            padding: 20,
        },
        typography: {
            width: 400,
            height: 150,
            textAlign: 'center',
        },
    }),
)

const ErrorPage = () => {
    const classes = useStyles()
    const history = useHistory()

    const handleGoHome = () => history.replace(ROUTE_LIST)

    return (
        <PageFrame titleKey="ErrorPage.title">
            <Paper className={classes.paper}>
                <Typography className={classes.typography} color="error">
                    <FormattedMessage id="ErrorPage.message" />
                </Typography>
                <Button variant="contained" onClick={handleGoHome}>
                    <FormattedMessage id="ErrorPage.goHome" />
                </Button>
            </Paper>
        </PageFrame>
    )
}

export default ErrorPage
