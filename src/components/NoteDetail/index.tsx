import React, { useEffect, useRef, useState } from 'react'
import {
    Card,
    CardActions,
    CardContent,
    CircularProgress,
    createStyles,
    IconButton,
    Typography,
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import { ArrowLeft as ArrowLeftIcon, Delete as DeleteIcon, Edit as EditIcon } from '@material-ui/icons'
import { connect, ResolveThunks } from 'react-redux'
import { RouteComponentProps } from 'react-router'
import { FormattedMessage } from 'react-intl'
import PageFrame from '../common/PageFrame'
import { StoreState } from '../../store/createInitialState'
import { selectNoteById, selectNoteIdsInOrder, selectNotesLoading } from '../../store/notes/selectors'
import { ROUTE_LIST } from '../../utils/routeUtils'
import { sagaLoadNote, sagaLoadNotes } from '../../store/notes/sagaActions'
import { NoteMo } from '../../model/NoteMo'
import { deleteNoteThunk, updateNoteThunk } from '../../store/notes/thunks'
import NoteEditDialog from '../common/NoteEditDialog'
import ConfirmationDialog from '../common/ConfirmationDialog'
import componentTestIds from './componentTestIds'

interface Params {
    readonly noteIdStr: string
}

type OwnProps = RouteComponentProps<Params>

type Props = OwnProps & ReturnType<typeof mapStateToProps> & ResolveThunks<typeof mapDispatchToProps>

interface StyleProps {
    readonly note?: NoteMo
}

const useStyles = makeStyles(() =>
    createStyles<string, StyleProps>({
        // Demonstration of making style based on component state/props
        cardContent: (props) => ({
            textAlign: props.note ? 'left' : 'center',
            width: 400,
            minHeight: 200,
        }),
        noteTitle: {
            whiteSpace: 'pre',
        },
        actions: {
            display: 'flex',
        },
        leftHolder: {
            flex: 1,
        },
    }),
)

export const NoteDetail = ({
    noteId,
    note,
    noteIds,
    history,
    notesLoading,
    loadNotes,
    loadNote,
    updateNote,
    deleteNote,
}: Props) => {
    const classes = useStyles({ note })
    // Here we are using ref to store state variable which should not trigger re-render
    const alreadyLoadedRef = useRef(false)
    const [editDialogShown, setEditDialogShown] = useState(false)
    const [deleteDialogShown, setDeleteDialogShown] = useState(false)

    useEffect(() => {
        if (!alreadyLoadedRef.current) {
            // Load just once
            alreadyLoadedRef.current = true

            if (noteIds.length > 0) {
                // Have some notes - reload just this one note
                loadNote(noteId)
            } else {
                // Have no notes - populate
                loadNotes()
            }
        }
    }, [note, noteIds, noteId, alreadyLoadedRef, loadNotes, loadNote])

    const handleBack = () => {
        // It would be good to use history.goBack() but we don't know whether user came here from the list or directly...
        history.push(ROUTE_LIST)
    }
    const handleShowEditDialog = () => setEditDialogShown(true)
    const handleHideEditDialog = () => setEditDialogShown(false)
    const handleUpdateNote = async (title: string) => {
        await updateNote({ id: noteId, title })
        setEditDialogShown(false)
    }

    const handleShowDeleteDialog = () => setDeleteDialogShown(true)
    const handleHideDeleteDialog = () => setDeleteDialogShown(false)
    const handleDeleteNote = async () => {
        await deleteNote(noteId)
        setDeleteDialogShown(false)
        handleBack()
    }

    const showLoading = notesLoading && !note

    return (
        <PageFrame titleKey="Header.noteDetail">
            <Card>
                <CardContent className={classes.cardContent}>
                    {showLoading && <CircularProgress />}
                    {!showLoading && (
                        <Typography color={note ? 'inherit' : 'error'} className={classes.noteTitle}>
                            {note ? note.title : <FormattedMessage id="NoteDetail.error" />}
                        </Typography>
                    )}
                </CardContent>
                <CardActions className={classes.actions}>
                    <div className={classes.leftHolder}>
                        <IconButton onClick={handleBack}>
                            <ArrowLeftIcon />
                        </IconButton>
                    </div>
                    <IconButton onClick={handleShowEditDialog} disabled={!note}>
                        <EditIcon />
                    </IconButton>
                    <IconButton
                        onClick={handleShowDeleteDialog}
                        disabled={!note}
                        data-testid={componentTestIds.deleteButton}
                    >
                        <DeleteIcon />
                    </IconButton>
                </CardActions>
            </Card>
            {editDialogShown && (
                <NoteEditDialog
                    saveButtonKey="NoteEditDialog.update"
                    initialTitle={note?.title}
                    onCancel={handleHideEditDialog}
                    onSave={handleUpdateNote}
                />
            )}
            {deleteDialogShown && (
                <ConfirmationDialog
                    confirmButtonKey="NoteDetail.delete"
                    onCancel={handleHideDeleteDialog}
                    onConfirm={handleDeleteNote}
                >
                    <FormattedMessage id="NoteDetail.confirmDelete" />
                </ConfirmationDialog>
            )}
        </PageFrame>
    )
}

const mapStateToProps = (state: StoreState, props: OwnProps) => {
    const noteId = Number(props.match.params.noteIdStr)
    return {
        note: selectNoteById(noteId)(state),
        noteIds: selectNoteIdsInOrder(state),
        notesLoading: selectNotesLoading(state),
        noteId,
    }
}

const mapDispatchToProps = {
    loadNotes: sagaLoadNotes,
    loadNote: sagaLoadNote,
    updateNote: updateNoteThunk,
    deleteNote: deleteNoteThunk,
}

export default connect(mapStateToProps, mapDispatchToProps)(NoteDetail)
