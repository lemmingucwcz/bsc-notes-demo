import React from 'react'
import { act, render, waitFor } from '@testing-library/react'
import { History, Location } from 'history'
import { Provider } from 'react-redux'
import configureMockStore from 'redux-mock-store'
import { IntlProvider } from 'react-intl'
import { NoteDetail } from '../index'
import createInitialState from '../../../store/createInitialState'

import enMessages from '../../../locale/en.json'
import { NoteMo } from '../../../model/NoteMo'
import componentTestIds from '../../componentTestIds'
import { ROUTE_LIST } from '../../../utils/routeUtils'

// We can afford any in tests and it makes our lives a lot easier
/* eslint-disable @typescript-eslint/no-explicit-any */

const mockStore = configureMockStore()
const store = mockStore(createInitialState())

const TW: React.FC<{}> = ({ children }) => (
    <Provider store={store}>
        <IntlProvider locale="en" messages={enMessages}>
            {children}
        </IntlProvider>
    </Provider>
)

const mockNote: NoteMo = {
    id: 2,
    title: 'Test',
}

describe('NoteDetail', () => {
    const mockLoadNote = jest.fn().mockResolvedValue(mockNote)
    const mockLoadNotes = jest.fn().mockResolvedValue([])
    const mockHistory: History = {
        push: jest.fn(),
    } as any
    const mockUpdateNote = jest.fn().mockResolvedValue(mockNote)
    const mockDeleteNote = jest.fn().mockResolvedValue({})

    beforeEach(jest.clearAllMocks)

    const baseProps = {
        noteId: 1,
        note: undefined,
        noteIds: [],
        history: mockHistory,
        notesLoading: false,
        loadNotes: mockLoadNotes,
        loadNote: mockLoadNote,
        updateNote: mockUpdateNote,
        deleteNote: mockDeleteNote,
        location: {} as Location,
        match: {} as any,
    }

    it('should call loadNotes when called with no note ids', () => {
        render(
            <TW>
                {/* eslint-disable-next-line react/jsx-props-no-spreading */}
                <NoteDetail {...baseProps} />
            </TW>,
        )

        expect(mockLoadNotes).toHaveBeenCalled()
    })

    it('should call loadNote with note id when called with note ids', () => {
        render(
            <TW>
                {/* eslint-disable-next-line react/jsx-props-no-spreading */}
                <NoteDetail {...baseProps} noteId={2} noteIds={[2]} />
            </TW>,
        )

        expect(mockLoadNote).toHaveBeenCalledWith(2)
    })

    it('should delete note and go back after note is successfully deleted', async () => {
        const wrapper = render(
            <TW>
                {/* eslint-disable-next-line react/jsx-props-no-spreading */}
                <NoteDetail {...baseProps} noteId={2} noteIds={[2]} note={mockNote} />
            </TW>,
        )

        // Click on delete button
        const deleteButton = await wrapper.findByTestId(componentTestIds.NoteDetail.deleteButton)
        expect(deleteButton).toBeDefined()
        act(() => deleteButton.click())

        // Click on confirm button
        const confirmButton = await wrapper.findByTestId(componentTestIds.ConfirmationDialog.confirmButton)
        expect(confirmButton).toBeDefined()
        act(() => confirmButton.click())

        await waitFor(() => expect(mockDeleteNote).toHaveBeenCalledWith(2))
        await waitFor(() => expect(mockHistory.push).toHaveBeenCalledWith(ROUTE_LIST))
    })
})
