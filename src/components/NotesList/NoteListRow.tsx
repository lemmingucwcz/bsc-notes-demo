import { IconButton, makeStyles, TableCell, TableRow } from '@material-ui/core'
import React from 'react'
import { ArrowRight as ArrowRightIcon } from '@material-ui/icons'
import { connect } from 'react-redux'
import { useHistory } from 'react-router'
import { createStyles } from '@material-ui/core/styles'
import { StoreState } from '../../store/createInitialState'
import { selectNoteById } from '../../store/notes/selectors'
import { ROUTE_DETAIL } from '../../utils/routeUtils'

interface OwnProps {
    readonly id: number
}

const useStyles = makeStyles(() =>
    createStyles({
        noteTitle: {
            display: 'block',
            width: 380,
            whiteSpace: 'nowrap',
            overflow: 'hidden',
            textOverflow: 'ellipsis',
        },
        titleCell: {
            cursor: 'pointer',
        },
    }),
)

const NoteListRow = ({ note }: ReturnType<typeof mapStateToProps>) => {
    const classes = useStyles()
    const history = useHistory()

    const handleGoToDetail = () => history.push(ROUTE_DETAIL(note?.id || 0))

    return (
        <TableRow>
            <TableCell variant="body" className={classes.titleCell} onClick={handleGoToDetail}>
                <span className={classes.noteTitle}>{note?.title}</span>
            </TableCell>
            <TableCell variant="body">
                <IconButton edge="end" onClick={handleGoToDetail}>
                    <ArrowRightIcon />
                </IconButton>
            </TableCell>
        </TableRow>
    )
}

const mapStateToProps = (state: StoreState, { id }: OwnProps) => ({ note: selectNoteById(id)(state) })

export default connect(mapStateToProps)(NoteListRow)
