import React from 'react'
import { IconButton, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@material-ui/core'
import { FormattedMessage } from 'react-intl'
import { Refresh as RefreshIcon } from '@material-ui/icons'
import { createStyles, makeStyles } from '@material-ui/core/styles'
import NoteListRow from './NoteListRow'

interface Props {
    readonly noteIds: number[]
    readonly onRefresh: () => void
}

const useStyles = makeStyles(() =>
    createStyles({
        tableContainer: {
            width: 500,
        },
        secondColumn: {
            width: 60,
        },
    }),
)

const NotesTable = ({ noteIds, onRefresh }: Props) => {
    const classes = useStyles()

    return (
        <TableContainer component={Paper} className={classes.tableContainer}>
            <Table>
                <TableHead>
                    <TableRow>
                        <TableCell>
                            <FormattedMessage id="Note.title" />
                        </TableCell>
                        <TableCell className={classes.secondColumn}>
                            <IconButton onClick={onRefresh}>
                                <RefreshIcon />
                            </IconButton>
                        </TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {noteIds.map((id) => (
                        <NoteListRow key={id} id={id} />
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    )
}

export default NotesTable
