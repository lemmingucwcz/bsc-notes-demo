import React, { useEffect, useState } from 'react'
import { FormattedMessage } from 'react-intl'
import { CircularProgress, Fab, Typography } from '@material-ui/core'
import { connect, ResolveThunks } from 'react-redux'
import { createStyles, makeStyles } from '@material-ui/core/styles'
import { Add as AddIcon } from '@material-ui/icons'
import PageFrame from '../common/PageFrame'
import { StoreState } from '../../store/createInitialState'
import { selectNoteIdsInOrder, selectNotesCharacterCount, selectNotesLoading } from '../../store/notes/selectors'
import { sagaLoadNotes } from '../../store/notes/sagaActions'
import NotesTable from './NotesTable'
import { createNoteThunk } from '../../store/notes/thunks'
import NoteEditDialog from '../common/NoteEditDialog'

const useStyles = makeStyles(() =>
    createStyles({
        stats: {
            display: 'flex',
            width: '100%',
            justifyContent: 'space-between',
            padding: 20,
        },
        loadingContainer: {
            width: 500,
            textAlign: 'center',
        },
        addButton: {
            position: 'fixed',
            right: '2vw',
            bottom: '2vw',
        },
    }),
)

const NotesList = ({
    noteIds,
    characterCount,
    notesLoading,
    loadNotes,
    createNote,
}: ReturnType<typeof mapStateToProps> & ResolveThunks<typeof mapDispatchToProps>) => {
    const classes = useStyles()
    const [createDialogOpen, setCreateDialogOpen] = useState(false)

    // Load notes on first render
    useEffect(() => {
        if (noteIds.length === 0) {
            loadNotes()
        }
    }, [loadNotes, noteIds])

    const handleOpenCreateDialog = () => setCreateDialogOpen(true)
    const handleCloseCreateDialog = () => setCreateDialogOpen(false)

    const handleSave = async (title: string) => {
        await createNote(title)
        setCreateDialogOpen(false)
    }

    return (
        <PageFrame titleKey="Header.notesList">
            <div>
                <div className={classes.stats}>
                    <Typography>
                        <FormattedMessage id="NoteList.notesCount" values={{ count: noteIds.length }} />
                    </Typography>
                    <Typography>
                        <FormattedMessage id="NoteList.notesLength" values={{ characterCount }} />
                    </Typography>
                </div>

                {notesLoading && (
                    <div className={classes.loadingContainer}>
                        <CircularProgress />
                    </div>
                )}
                {!notesLoading && <NotesTable noteIds={noteIds} onRefresh={loadNotes} />}
                <Fab color="primary" className={classes.addButton} onClick={handleOpenCreateDialog}>
                    <AddIcon />
                </Fab>
                {createDialogOpen && (
                    <NoteEditDialog
                        saveButtonKey="NoteEditDialog.create"
                        onCancel={handleCloseCreateDialog}
                        onSave={handleSave}
                    />
                )}
            </div>
        </PageFrame>
    )
}

const mapStateToProps = (state: StoreState) => ({
    noteIds: selectNoteIdsInOrder(state),
    characterCount: selectNotesCharacterCount(state),
    notesLoading: selectNotesLoading(state),
})

const mapDispatchToProps = {
    loadNotes: sagaLoadNotes,
    createNote: createNoteThunk,
}

export default connect(mapStateToProps, mapDispatchToProps)(NotesList)
