import React from 'react'
import { Snackbar } from '@material-ui/core'
import { Alert } from '@material-ui/lab'
import { FormattedMessage } from 'react-intl'
import { connect } from 'react-redux'
import { StoreState } from '../../../store/createInitialState'
import { selectApiErrorShown } from '../../../store/api/selectors'
import { setApiErrorShown } from '../../../store/api/actions'

const CommunicationAlert = ({
    apiErrorShown,
    hideApiError,
}: ReturnType<typeof mapStateToProps> & typeof mapDispatchToProps) => (
    <Snackbar open={apiErrorShown} onClose={hideApiError}>
        <Alert severity="error" elevation={6} variant="filled" onClose={hideApiError}>
            <FormattedMessage id="CommunicationAlert.hadApiError" />
        </Alert>
    </Snackbar>
)

const mapStateToProps = (state: StoreState) => ({
    apiErrorShown: selectApiErrorShown(state),
})

const mapDispatchToProps = {
    hideApiError: () => setApiErrorShown(false),
}

export default connect(mapStateToProps, mapDispatchToProps)(CommunicationAlert)
