import React, { useState } from 'react'
import { createStyles, makeStyles } from '@material-ui/core/styles'
import { Button, CircularProgress, Dialog, Paper, Typography } from '@material-ui/core'
import { FormattedMessage } from 'react-intl'
import componentTestIds from './componentTestIds'

interface Props {
    readonly confirmButtonKey?: string
    readonly onCancel: () => void
    readonly onConfirm: () => Promise<void>
}

const useStyles = makeStyles(() =>
    createStyles({
        dialogPaper: {
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            padding: 20,
        },
        message: {
            minHeight: 100,
            minWidth: 250,
            maxWidth: 400,
            width: '100%',
        },
        buttons: {
            marginTop: 20,
        },
        button: {
            margin: '0 10px',
            width: 120,
        },
        progress: {
            marginRight: 10,
        },
    }),
)

const ConfirmationDialog: React.FC<Props> = ({
    confirmButtonKey = 'ConfirmationDialog.confirm',
    children,
    onCancel,
    onConfirm,
}) => {
    const classes = useStyles()
    const [saving, setSaving] = useState(false)

    const handleSave = () => {
        setSaving(true)
        // Clear saving flag no matter what the result
        onConfirm().catch(() => {
            // Error already shown by thunk so just stop the indicator
            setSaving(false)
        })
    }

    return (
        <Dialog open onClose={onCancel}>
            <Paper className={classes.dialogPaper}>
                <Typography className={classes.message}>{children}</Typography>
                <div className={classes.buttons}>
                    <Button
                        variant="text"
                        className={classes.button}
                        onClick={onCancel}
                        disabled={saving}
                        data-testid={componentTestIds.cancelButton}
                    >
                        <FormattedMessage id="ConfirmationDialog.cancel" />
                    </Button>
                    <Button
                        variant="contained"
                        className={classes.button}
                        color="primary"
                        disabled={saving}
                        onClick={handleSave}
                        data-testid={componentTestIds.confirmButton}
                    >
                        {saving && <CircularProgress size={15} color="inherit" className={classes.progress} />}
                        <FormattedMessage id={confirmButtonKey} />
                    </Button>
                </div>
            </Paper>
        </Dialog>
    )
}

export default ConfirmationDialog
