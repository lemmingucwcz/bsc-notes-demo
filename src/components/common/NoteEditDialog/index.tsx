import React, { ChangeEvent, useState } from 'react'
import { createStyles, makeStyles } from '@material-ui/core/styles'
import { Button, CircularProgress, Dialog, Paper, TextField } from '@material-ui/core'
import { FormattedMessage } from 'react-intl'

interface Props {
    readonly initialTitle?: string
    readonly saveButtonKey: string
    readonly onCancel: () => void
    readonly onSave: (title: string) => Promise<void>
}

const useStyles = makeStyles(() =>
    createStyles({
        dialogPaper: {
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            padding: 20,
        },
        textField: {
            width: 400,
        },
        buttons: {
            marginTop: 20,
        },
        button: {
            margin: '0 10px',
            width: 120,
        },
        progress: {
            marginRight: 10,
        },
    }),
)

const NoteEditDialog = ({ initialTitle = '', saveButtonKey, onCancel, onSave }: Props) => {
    const classes = useStyles()
    const [title, setTitle] = useState(initialTitle)
    const [saving, setSaving] = useState(false)

    const handleChange = (event: ChangeEvent<HTMLTextAreaElement | HTMLInputElement>) => setTitle(event.target.value)

    const handleSave = () => {
        setSaving(true)
        // Clear saving flag no matter what the result
        onSave(title).catch(() => {
            // Error already shown by thunk so just stop the indicator
            setSaving(false)
        })
    }

    return (
        <Dialog open onClose={onCancel}>
            <Paper className={classes.dialogPaper}>
                <TextField
                    multiline
                    disabled={saving}
                    className={classes.textField}
                    value={title}
                    onChange={handleChange}
                    rows={10}
                    label={<FormattedMessage id="Note.title" />}
                    autoFocus
                />
                <div className={classes.buttons}>
                    <Button variant="text" className={classes.button} onClick={onCancel} disabled={saving}>
                        <FormattedMessage id="NoteEditDialog.cancel" />
                    </Button>
                    <Button
                        variant="contained"
                        className={classes.button}
                        color="primary"
                        onClick={handleSave}
                        disabled={saving || !title}
                    >
                        {saving && <CircularProgress size={15} color="inherit" className={classes.progress} />}

                        <FormattedMessage id={saveButtonKey} />
                    </Button>
                </div>
            </Paper>
        </Dialog>
    )
}

export default NoteEditDialog
