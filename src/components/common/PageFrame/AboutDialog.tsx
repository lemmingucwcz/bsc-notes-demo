import React from 'react'
import { createStyles, makeStyles } from '@material-ui/core/styles'
import { Dialog, Paper, Typography } from '@material-ui/core'
import { FormattedMessage } from 'react-intl'
import Button from '@material-ui/core/Button'

interface Props {
    readonly open: boolean
    readonly onClose: () => void
}

const useStyles = makeStyles(() =>
    createStyles({
        dialogPaper: {
            padding: 20,
            textAlign: 'center',
        },
        dialogText: {
            marginBottom: 20,
        },
    }),
)

const AboutDialog = ({ open, onClose }: Props) => {
    const classes = useStyles()

    return (
        <Dialog open={open} onClose={onClose}>
            <Paper className={classes.dialogPaper}>
                <Typography className={classes.dialogText}>
                    <FormattedMessage id="About.about" />
                </Typography>
                <Button variant="contained" color="primary" onClick={onClose}>
                    <FormattedMessage id="About.close" />
                </Button>
            </Paper>
        </Dialog>
    )
}

export default AboutDialog
