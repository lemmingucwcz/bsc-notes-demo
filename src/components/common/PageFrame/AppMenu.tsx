import React, { useState } from 'react'
import { Drawer, List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core'
import { FormattedMessage } from 'react-intl'
import { Info as IconInfo } from '@material-ui/icons'
import { createStyles, makeStyles } from '@material-ui/core/styles'
import AboutDialog from './AboutDialog'

interface Props {
    readonly open: boolean
    readonly onClose: () => void
}

const useStyles = makeStyles(() =>
    createStyles({
        list: {
            width: 250,
        },
        dialogPaper: {
            padding: 20,
            textAlign: 'center',
        },
        dialogText: {
            marginBottom: 20,
        },
    }),
)

const AppMenu = ({ open, onClose }: Props) => {
    const classes = useStyles()
    const [aboutShown, setAboutShown] = useState(false)

    const handleShowAbout = () => setAboutShown(true)
    const handleHideAbout = () => setAboutShown(false)

    return (
        <>
            <Drawer anchor="left" open={open} onClose={onClose}>
                <List className={classes.list}>
                    <ListItem button onClick={handleShowAbout}>
                        <ListItemIcon>
                            <IconInfo />
                        </ListItemIcon>
                        <ListItemText primary={<FormattedMessage id="AppMenu.about" />} />
                    </ListItem>
                </List>
            </Drawer>
            <AboutDialog open={aboutShown} onClose={handleHideAbout} />
        </>
    )
}

export default AppMenu
