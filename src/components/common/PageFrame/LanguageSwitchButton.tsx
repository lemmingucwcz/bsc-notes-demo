import React, { MouseEvent, useState } from 'react'
import { Button, ListItemText, Menu, MenuItem } from '@material-ui/core'
import { connect } from 'react-redux'
import { KnownLocale, knownLocales } from '../../../store/locale/store'
import { StoreState } from '../../../store/createInitialState'
import { selectCurrentLocale } from '../../../store/locale/selectors'
import { sagaSetCurrentLocale } from '../../../store/locale/sagaActions'

const LanguageSwitchButton = ({
    setCurrentLocale,
    currentLocale,
}: ReturnType<typeof mapStateToProps> & typeof mapDispatchToProps) => {
    const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | undefined>(undefined)

    const handleClick = (event: MouseEvent<HTMLButtonElement>) => {
        setAnchorEl(event.currentTarget)
    }

    const handleClose = () => {
        setAnchorEl(undefined)
    }

    const handleMenuClick = (locale: KnownLocale) => () => {
        setCurrentLocale(locale)
        handleClose()
    }

    return (
        <>
            <Button
                aria-controls="customized-menu"
                aria-haspopup="true"
                variant="contained"
                color="secondary"
                onClick={handleClick}
            >
                {currentLocale}
            </Button>

            <Menu anchorEl={anchorEl} open={Boolean(anchorEl)} onClose={handleClose} keepMounted>
                {knownLocales.map((locale) => (
                    <MenuItem key={locale} onClick={handleMenuClick(locale)}>
                        <ListItemText primary={locale.toUpperCase()} />
                    </MenuItem>
                ))}
            </Menu>
        </>
    )
}

const mapStateToProps = (state: StoreState) => ({
    currentLocale: selectCurrentLocale(state),
})

const mapDispatchToProps = {
    setCurrentLocale: sagaSetCurrentLocale,
}

export default connect(mapStateToProps, mapDispatchToProps)(LanguageSwitchButton)
