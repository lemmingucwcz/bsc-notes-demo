import React from 'react'
import { IconButton, Toolbar, Typography } from '@material-ui/core'
import { FormattedMessage } from 'react-intl'
import { createStyles, makeStyles } from '@material-ui/core/styles'
import MenuIcon from '@material-ui/icons/Menu'
import LanguageSwitchButton from './LanguageSwitchButton'

const useStyles = makeStyles(() =>
    createStyles({
        toolbar: {
            display: 'flex',
            justifyContent: 'space-between',
        },
        left: {
            display: 'flex',
            alignItems: 'center',
        },
        menuButton: {
            marginRight: 10,
        },
    }),
)

interface Props {
    readonly titleKey: string
    readonly onToggleMenu: () => void
}

const PageToolbar = ({ titleKey, onToggleMenu }: Props) => {
    const classes = useStyles()

    return (
        <Toolbar className={classes.toolbar}>
            <div className={classes.left}>
                <IconButton
                    edge="start"
                    color="inherit"
                    aria-label="menu"
                    className={classes.menuButton}
                    onClick={onToggleMenu}
                >
                    <MenuIcon />
                </IconButton>
                <Typography variant="h4">
                    <FormattedMessage id={titleKey} />
                </Typography>
            </div>
            <LanguageSwitchButton />
        </Toolbar>
    )
}

export default PageToolbar
