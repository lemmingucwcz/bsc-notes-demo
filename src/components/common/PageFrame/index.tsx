import React, { useState } from 'react'
import { AppBar, CssBaseline } from '@material-ui/core'
import { createStyles, makeStyles } from '@material-ui/core/styles'
import PageToolbar from './PageToolbar'
import AppMenu from './AppMenu'
import CommunicationAlert from '../CommunicationAlert'

interface Props {
    readonly titleKey: string
}

const useStyles = makeStyles(() =>
    createStyles({
        pageWrapper: {
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            padding: 20,
        },
    }),
)

const PageFrame: React.FC<Props> = ({ titleKey, children }) => {
    const classes = useStyles()
    const [menuOpen, setMenuOpen] = useState(false)

    const handleToggleMenuOpen = () => setMenuOpen((value) => !value)

    return (
        <>
            <CssBaseline />
            <AppBar position="sticky">
                <PageToolbar titleKey={titleKey} onToggleMenu={handleToggleMenuOpen} />
            </AppBar>
            <AppMenu open={menuOpen} onClose={handleToggleMenuOpen} />
            <main className={classes.pageWrapper}>{children}</main>
            <CommunicationAlert />
        </>
    )
}

export default PageFrame
