import noteDetailComponentTestIds from './NoteDetail/componentTestIds'
import confirmationDialogComponentTestIds from './common/ConfirmationDialog/componentTestIds'

const componentTestIds = {
    NoteDetail: noteDetailComponentTestIds,
    ConfirmationDialog: confirmationDialogComponentTestIds,
}

export default componentTestIds
