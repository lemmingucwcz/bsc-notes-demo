import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'connected-react-router'
import { createBrowserHistory } from 'history'
import createAppStore from './store/createAppStore'
import createInitialState from './store/createInitialState'
import Routes from './routes'
import LocaleProvider from './locale/LocaleProvider'
import 'fontsource-roboto'
import 'fontsource-roboto/500.css'

const history = createBrowserHistory()
const store = createAppStore(createInitialState(), history)

ReactDOM.render(
    <Provider store={store}>
        <LocaleProvider>
            <ConnectedRouter history={history}>
                <Routes />
            </ConnectedRouter>
        </LocaleProvider>
    </Provider>,
    document.getElementById('root'),
)
