import React from 'react'
import { connect } from 'react-redux'
import { IntlProvider } from 'react-intl'
import { StoreState } from '../store/createInitialState'
import { selectCurrentLocale } from '../store/locale/selectors'

import csMessages from './cs.json'
import enMessages from './en.json'
import { knownLocales } from '../store/locale/store'

const messages = {
    cs: csMessages,
    en: enMessages,
}

// Check that locales in locale store and here match. We must copy knownLocales before sorting because
// sort() modifies original array.
if (Object.keys(messages).sort().join(',') !== [...knownLocales].sort().join(',')) {
    throw new Error('Loaded and declared locales do not match!')
}

const mapStateToProps = (state: StoreState) => ({
    currentLocale: selectCurrentLocale(state),
})
const LocaleProvider: React.FC<ReturnType<typeof mapStateToProps>> = ({ currentLocale, children }) => {
    return (
        <IntlProvider locale={currentLocale} messages={messages[currentLocale]} defaultLocale="en">
            {children}
        </IntlProvider>
    )
}

export default connect(mapStateToProps)(LocaleProvider)
