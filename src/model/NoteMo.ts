export interface NoteMo {
    readonly id: number
    readonly title: string
}
