import * as React from 'react'
import { Route, Switch } from 'react-router'
import NotesList from './components/NotesList'
import NoteDetail from './components/NoteDetail'
import ErrorPage from './components/ErrorPage'

export default () => (
    <Switch>
        <Route exact path="/" component={NotesList} />
        <Route exact path="/detail/:noteIdStr" component={NoteDetail} />
        <Route component={ErrorPage} />
    </Switch>
)
