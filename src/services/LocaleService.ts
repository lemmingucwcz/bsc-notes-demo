import { KnownLocale, knownLocales } from '../store/locale/store'

const LOCALE_KEY = 'SELECTED_LOCALE'

const LocaleService = {
    /**
     * Save locale selection to local storage
     *
     * @param locale Locale selection
     */
    saveLocale: (locale: KnownLocale) => {
        window.localStorage.setItem(LOCALE_KEY, locale)
    },

    /**
     * Load locale selection from local storage
     *
     * @return Saved locale (guarantted to be valid) or undefined when locale is not saved or valid
     */
    loadLocale: (): KnownLocale | undefined => {
        const stored = window.localStorage.getItem(LOCALE_KEY)

        // Only return locales we know
        return stored && (knownLocales as string[]).includes(stored) ? (stored as KnownLocale) : undefined
    },
}

export default LocaleService
