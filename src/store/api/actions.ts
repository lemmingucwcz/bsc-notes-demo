export const ACTION_SET_API_ERROR_SHOWN = 'api/apiErrorShown'

export const setApiErrorShown = (apiErrorShown: boolean) => ({
    type: ACTION_SET_API_ERROR_SHOWN as typeof ACTION_SET_API_ERROR_SHOWN,
    apiErrorShown,
})

export type ApiActionType = ReturnType<typeof setApiErrorShown>
