import { ApiStore } from './store'
import { ACTION_SET_API_ERROR_SHOWN, ApiActionType } from './actions'

const initialState: ApiStore = {
    apiErrorShown: false,
}

const apiReducer = (state: ApiStore = initialState, action?: ApiActionType) => {
    // This is so that TS knows action is defined in switch cases
    if (action) {
        switch (action.type) {
            case ACTION_SET_API_ERROR_SHOWN:
                return { apiErrorShown: action.apiErrorShown }
            default:
                return state
        }
    }
    return state
}

export default apiReducer
