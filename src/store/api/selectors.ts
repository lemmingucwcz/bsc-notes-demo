import { StoreState } from '../createInitialState'

// eslint-disable-next-line import/prefer-default-export
export const selectApiErrorShown = (store: StoreState) => store.api.apiErrorShown
