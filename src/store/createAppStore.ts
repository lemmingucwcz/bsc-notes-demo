import { createStore, applyMiddleware, StoreEnhancer } from 'redux'
import createSagaMiddleware from 'redux-saga'
import { History } from 'history'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk from 'redux-thunk'
import createRootReducer from './createRootReducer'
import { StoreState } from './createInitialState'
import notesSagas from './notes/sagas'
import localeSagas from './locale/sagas'

// Initiate sagas for all slices
function* runAllSliceSagas() {
    yield* notesSagas
    yield* localeSagas
}

// Connect to Redux DevTools only in development
const enhancerCompose =
    process.env.NODE_ENV === 'development' ? composeWithDevTools : (enhancer: StoreEnhancer) => enhancer

export default (initialState: StoreState, history: History) => {
    const sagaMiddleware = createSagaMiddleware()
    const store = createStore(createRootReducer(history), enhancerCompose(applyMiddleware(sagaMiddleware, thunk)))
    sagaMiddleware.run(runAllSliceSagas)

    return store
}
