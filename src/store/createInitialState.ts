import notesReducer from './notes/reducer'
import localeReducer from './locale/reducer'
import apiReducer from './api/reducer'

const createInitialState = () => ({
    // Call reducer with no params to return its initial state
    notes: notesReducer(),
    locale: localeReducer(),
    api: apiReducer(),
})

export type StoreState = ReturnType<typeof createInitialState>

export default createInitialState
