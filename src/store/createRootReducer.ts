import { connectRouter } from 'connected-react-router'
import { combineReducers } from 'redux'
import { History } from 'history'
import notesReducer from './notes/reducer'
import localeReducer from './locale/reducer'
import apiReducer from './api/reducer'

const createRootReducer = (history: History) =>
    combineReducers({
        router: connectRouter(history),
        notes: notesReducer,
        locale: localeReducer,
        api: apiReducer,
    })

export default createRootReducer
