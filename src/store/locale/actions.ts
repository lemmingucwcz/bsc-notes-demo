import { KnownLocale } from './store'

export const ACTION_SET_CURRENT_LOCALE = 'currentLocale/setCurrentLocale'

export const setCurrentLocale = (locale: KnownLocale) => ({
    type: ACTION_SET_CURRENT_LOCALE,
    locale,
})

export type LocaleActionType = ReturnType<typeof setCurrentLocale>
