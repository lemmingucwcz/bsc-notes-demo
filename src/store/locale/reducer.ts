import { LocaleStore } from './store'
import { ACTION_SET_CURRENT_LOCALE, LocaleActionType } from './actions'
import LocaleService from '../../services/LocaleService'

// Implemented as a function so it initializes currentLocale correctly even when
// we use initialState after locale has been changed in the app
const getInitialState = (): LocaleStore => ({
    // We could try to get preferred language from browser here, but it is not very interesting to demo
    currentLocale: LocaleService.loadLocale() || 'cs',
})

const localeReducer = (maybeState?: LocaleStore, action?: LocaleActionType) => {
    // We can't use function as default parameter so we have to do this
    const state = maybeState || getInitialState()

    // This is so that TS knows action is defined in switch cases
    if (action) {
        switch (action.type) {
            case ACTION_SET_CURRENT_LOCALE:
                return { currentLocale: action.locale }
            default:
                return state
        }
    }
    return state
}

export default localeReducer
