import { KnownLocale } from './store'

export const SAGA_SET_CURRENT_LOCALE = 'locale/sagaSetCurrentLocale'

export const sagaSetCurrentLocale = (locale: KnownLocale) => ({
    type: SAGA_SET_CURRENT_LOCALE,
    locale,
})
