import { call, put, takeEvery } from 'redux-saga/effects'
import { SAGA_SET_CURRENT_LOCALE, sagaSetCurrentLocale } from './sagaActions'
import { setCurrentLocale } from './actions'
import LocaleService from '../../services/LocaleService'

function* setCurrentLocaleSaga({ locale }: ReturnType<typeof sagaSetCurrentLocale>) {
    yield call(LocaleService.saveLocale, locale)
    yield put(setCurrentLocale(locale))
}

export default [takeEvery(SAGA_SET_CURRENT_LOCALE, setCurrentLocaleSaga)]
