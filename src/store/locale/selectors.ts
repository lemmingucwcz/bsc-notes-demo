import { StoreState } from '../createInitialState'

// eslint-disable-next-line import/prefer-default-export
export const selectCurrentLocale = (state: StoreState) => state.locale.currentLocale
