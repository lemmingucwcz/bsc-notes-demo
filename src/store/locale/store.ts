const locales = { en: true, cs: true }
export type KnownLocale = keyof typeof locales
export const knownLocales = Object.keys(locales) as KnownLocale[]

export interface LocaleStore {
    currentLocale: KnownLocale
}
