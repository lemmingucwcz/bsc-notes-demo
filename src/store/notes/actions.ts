import { NoteMo } from '../../model/NoteMo'

export const ACTION_SET_NOTES = 'notes/setNotes'
export const ACTION_UPSERT_NOTE = 'notes/upsertNote'
export const ACTION_DELETE_NOTE = 'notes/deleteNote'
export const ACTION_SET_LOADING = 'notes/setLoading'

/**
 * Set new list of notes
 *
 * @param notes New notes
 */
export const setNotes = (notes: NoteMo[]) => ({
    // Typecasts are needed so that type is string literal and not just a string
    type: ACTION_SET_NOTES as typeof ACTION_SET_NOTES,
    notes,
})

/**
 * Insert or update note
 *
 * @param note New / updated note
 */
export const upsertNote = (note: NoteMo) => ({
    type: ACTION_UPSERT_NOTE as typeof ACTION_UPSERT_NOTE,
    note,
})

/**
 * Delete note
 *
 * @param note Note to delete
 */
export const deleteNote = (noteId: number) => ({
    type: ACTION_DELETE_NOTE as typeof ACTION_DELETE_NOTE,
    noteId,
})

export const setLoading = (loading: boolean) => ({
    type: ACTION_SET_LOADING as typeof ACTION_SET_LOADING,
    loading,
})

export type NotesActionType =
    | ReturnType<typeof setNotes>
    | ReturnType<typeof upsertNote>
    | ReturnType<typeof deleteNote>
    | ReturnType<typeof setLoading>
