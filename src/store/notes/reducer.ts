import { Map as ImmutableMap } from 'immutable'
import {
    NotesActionType,
    ACTION_SET_NOTES,
    ACTION_UPSERT_NOTE,
    ACTION_DELETE_NOTE,
    ACTION_SET_LOADING,
} from './actions'
import { NotesStore } from './store'
import { NoteMo } from '../../model/NoteMo'

const initialState: NotesStore = {
    nextNoteId: 1,
    noteOrder: [],
    noteMap: ImmutableMap(),
    loading: false,
}

const notesReducer = (state: NotesStore = initialState, action?: NotesActionType): NotesStore => {
    // This is so that TS knows action is defined in switch cases
    if (action) {
        switch (action.type) {
            case ACTION_SET_NOTES:
                // We could compute these three with a single for loop. Or even compute them in the adapter.
                // But this way we can demonstrate using array functions :)
                return {
                    ...state,
                    nextNoteId: action.notes.reduce((max, note) => Math.max(max, note.id + 1), 1),
                    noteOrder: action.notes.map((note) => note.id).reverse(),
                    noteMap: ImmutableMap(
                        action.notes.reduce((map, note) => {
                            map.set(note.id, note)
                            return map
                        }, new Map<number, NoteMo>()),
                    ),
                }
            case ACTION_UPSERT_NOTE:
                return {
                    ...state,
                    nextNoteId: Math.max(state.nextNoteId, action.note.id + 1),
                    noteMap: state.noteMap.set(action.note.id, action.note),
                    noteOrder: state.noteOrder.includes(action.note.id)
                        ? state.noteOrder
                        : [action.note.id, ...state.noteOrder],
                }
            case ACTION_DELETE_NOTE:
                return {
                    ...state,
                    noteMap: state.noteMap.remove(action.noteId),
                    noteOrder: state.noteOrder.filter((id) => id !== action.noteId),
                }
            case ACTION_SET_LOADING:
                return { ...state, loading: action.loading }
            default:
                return state
        }
    }
    return state
}

export default notesReducer
