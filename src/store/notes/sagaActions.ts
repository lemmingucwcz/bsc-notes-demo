export const SAGA_ACTION_LOAD_NOTES = 'notes/loadNotes'
export const SAGA_ACTION_LOAD_NOTE = 'notes/loadNote'

/**
 * (Re)load notes
 *
 * @param notes New notes
 */
export const sagaLoadNotes = () => ({
    // Typecasts are needed so that type is string literal and not just a string
    type: SAGA_ACTION_LOAD_NOTES,
})

/**
 * Set new list of notes
 *
 * @param notes New notes
 */
export const sagaLoadNote = (noteId: number) => ({
    // Typecasts are needed so that type is string literal and not just a string
    type: SAGA_ACTION_LOAD_NOTE,
    noteId,
})
