import { call, cancel, Effect, put, takeEvery } from 'redux-saga/effects'

import { setLoading, setNotes, upsertNote } from './actions'
import { SAGA_ACTION_LOAD_NOTE, SAGA_ACTION_LOAD_NOTES, sagaLoadNote } from './sagaActions'
import NotesAdapter from '../../api/adapters/NotesAdapter'
import { setApiErrorShown } from '../api/actions'

/**
 * Helper to show error and cancel saga when API call fails
 *
 * @param effect Effect to wrap
 */
function* safe(effect: Effect) {
    try {
        return yield effect
    } catch (e) {
        // Show error and stop
        yield put(setApiErrorShown(true))
        yield put(setLoading(false))
        yield cancel()
        return undefined
    }
}

function* loadNotesSaga() {
    yield put(setLoading(true))
    const data = yield safe(call(NotesAdapter.list))
    yield put(setNotes(data))
    yield put(setLoading(false))
}

function* loadNoteSaga({ noteId }: ReturnType<typeof sagaLoadNote>) {
    yield put(setLoading(true))
    const data = yield safe(call(NotesAdapter.get, noteId))
    // Since mock API always returns note number 2, this will revert changes in that note
    // or add it when it was deleted any time note detail is shown.
    yield put(upsertNote(data))
    yield put(setLoading(false))
}

export default [takeEvery(SAGA_ACTION_LOAD_NOTES, loadNotesSaga), takeEvery(SAGA_ACTION_LOAD_NOTE, loadNoteSaga)]
