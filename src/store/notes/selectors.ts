import { createSelector } from 'reselect'
import { StoreState } from '../createInitialState'
import { NoteMo } from '../../model/NoteMo'

export const selectNextNoteId = (state: StoreState) => state.notes.nextNoteId

export const selectNoteIdsInOrder = (state: StoreState) => state.notes.noteOrder

export const selectNoteById = (id: number) => (state: StoreState): NoteMo | undefined => state.notes.noteMap.get(id)

export const selectNoteMap = (state: StoreState) => state.notes.noteMap

export const selectNotesLoading = (state: StoreState) => state.notes.loading

/**
 * Demonstration how to use reselect library to create caching selectors to improve performance
 * when selector does expensive computation
 */
export const selectNotesCharacterCount = createSelector(selectNoteMap, (noteMap) =>
    noteMap.reduce<number>((length, note) => (length || 0) + (note?.title.length || 0), 0),
)
