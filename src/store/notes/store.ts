import { Map as ImmutableMap } from 'immutable'
import { NoteMo } from '../../model/NoteMo'

export interface NotesStore {
    /**
     * Note IDs in the order they should be shown
     */
    readonly noteOrder: number[]

    /**
     * Map of note id => note
     *
     * (Could by done in other ways, but let's demonstrate use of Immutable.js)
     */
    readonly noteMap: ImmutableMap<number, NoteMo>

    /**
     * What it to assing to next created note
     */
    readonly nextNoteId: number

    /**
     * Notes are loading
     */
    readonly loading: boolean
}
