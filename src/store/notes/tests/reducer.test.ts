import { NoteMo } from '../../../model/NoteMo'
import notesReducer from '../reducer'
import { deleteNote, NotesActionType, setLoading, setNotes, upsertNote } from '../actions'

const mockNote1: NoteMo = {
    id: 1,
    title: 'Note 1',
}

const mockNote2: NoteMo = {
    id: 2,
    title: 'Note 2',
}

const mockNote3: NoteMo = {
    id: 3,
    title: 'Note 3',
}

describe('Notes reducer', () => {
    const initialState = notesReducer(undefined, setNotes([mockNote1, mockNote2]))

    describe('State init', () => {
        test('init without an action', () => {
            const result = notesReducer(undefined, undefined)

            expect(result.nextNoteId).toEqual(1)
            expect(result.noteOrder).toEqual([])
            expect(result.noteMap.size).toEqual(0)
        })

        test('init with unknown action', () => {
            // Uses another code branch
            const result = notesReducer(undefined, {} as NotesActionType)

            expect(result.nextNoteId).toEqual(1)
            expect(result.noteOrder).toEqual([])
            expect(result.noteMap.size).toEqual(0)
        })
    })

    describe('ACTION_SET_NOTES', () => {
        it('should set notes', () => {
            const result = notesReducer(undefined, setNotes([mockNote1, mockNote2]))

            expect(result.nextNoteId).toEqual(3)
            expect(result.noteOrder).toEqual([2, 1])
            expect(result.noteMap.size).toEqual(2)
            expect(result.noteMap.get(1)).toEqual(mockNote1)
            expect(result.noteMap.get(2)).toEqual(mockNote2)
        })
    })

    describe('ACTION_UPSERT_NOTE', () => {
        it('should add new note', () => {
            const result = notesReducer(initialState, upsertNote(mockNote3))
            expect(result.nextNoteId).toEqual(4)
            expect(result.noteOrder).toEqual([3, 2, 1])
            expect(result.noteMap.size).toEqual(3)
            expect(result.noteMap.get(3)).toEqual(mockNote3)
        })

        it('should update existing note', () => {
            const newNote = { id: 1, title: 'New note 1' }
            const result = notesReducer(initialState, upsertNote(newNote))
            expect(result.nextNoteId).toEqual(3)
            expect(result.noteOrder).toEqual([2, 1])
            expect(result.noteMap.size).toEqual(2)
            expect(result.noteMap.get(1)).toEqual(newNote)

            // State should be immutable - map in old state should still return old value!
            expect(initialState.noteMap.get(1)).toEqual(mockNote1)
        })
    })

    describe('ACTION_DELETE_NOTE', () => {
        it('should delete note', () => {
            const result = notesReducer(initialState, deleteNote(mockNote1.id))

            expect(result.nextNoteId).toEqual(3)
            expect(result.noteOrder).toEqual([2])
            expect(result.noteMap.size).toEqual(1)
            expect(result.noteMap.get(1)).toBeUndefined()
        })

        it('should do nothing when note id does not exist', () => {
            const result = notesReducer(initialState, deleteNote(999))

            expect(result.nextNoteId).toEqual(3)
            expect(result.noteOrder).toEqual([2, 1])
            expect(result.noteMap.size).toEqual(2)
        })
    })

    describe('ACTION_SET_LOADING', () => {
        it('should set loading', () => {
            const result = notesReducer(initialState, setLoading(true))

            expect(initialState.loading).toBeFalsy()
            expect(result.loading).toBeTruthy()
        })
    })
})
