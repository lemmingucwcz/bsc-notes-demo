import SagaTester from 'redux-saga-tester'
import { createBrowserHistory } from 'history'
import { Reducer } from 'redux'
import NotesAdapter from '../../../api/adapters/NotesAdapter'
import createInitialState, { StoreState } from '../../createInitialState'
import createRootReducer from '../../createRootReducer'
import nodesSagas from '../sagas'
import { sagaLoadNote, sagaLoadNotes } from '../sagaActions'
import { ACTION_SET_LOADING, ACTION_UPSERT_NOTE } from '../actions'
import { selectNoteById, selectNoteIdsInOrder, selectNotesLoading } from '../selectors'
import { selectApiErrorShown } from '../../api/selectors'

const history = createBrowserHistory()

// Typecast needed because of router slice that is added automagically
const rootReducer = (createRootReducer(history) as unknown) as Reducer<StoreState>
const initialState = createInitialState()

function* testSagas() {
    yield* nodesSagas
}

const initTester = () => {
    const sagaTester = new SagaTester<StoreState>({
        initialState,
        reducers: rootReducer,
    })

    sagaTester.start(testSagas)

    return sagaTester
}

describe('sagas', () => {
    const mockNote1 = { id: 1, title: 'Note 1' }
    const mockNote2 = { id: 2, title: 'Note 2' }
    const mockNote22 = { id: 2, title: 'Note 2.2' }

    describe('loadNotesSaga', () => {
        it('should store loaded values', async () => {
            jest.spyOn(NotesAdapter, 'list').mockResolvedValue([mockNote1, mockNote2])

            const sagaTester = initTester()

            sagaTester.dispatch(sagaLoadNotes())

            await sagaTester.waitFor(ACTION_SET_LOADING)

            const state = sagaTester.getState()
            expect(selectNoteIdsInOrder(state)).toEqual([2, 1])
            expect(selectNoteById(1)(state)).toEqual(mockNote1)
            expect(selectNoteById(2)(state)).toEqual(mockNote2)
            expect(selectApiErrorShown(state)).toBeFalsy()
            expect(selectNotesLoading(state)).toBeFalsy()
        })

        it('should show error when exception is thrown', async () => {
            jest.spyOn(NotesAdapter, 'list').mockRejectedValue({})

            const sagaTester = initTester()

            sagaTester.dispatch(sagaLoadNotes())

            await sagaTester.waitFor(ACTION_SET_LOADING)

            const state = sagaTester.getState()
            expect(selectNoteIdsInOrder(state)).toEqual([])
            expect(selectApiErrorShown(state)).toBeTruthy()
            expect(selectNotesLoading(state)).toBeFalsy()
        })
    })

    describe('loadNoteSaga', () => {
        it('should store loaded value', async () => {
            const spy = jest.spyOn(NotesAdapter, 'get').mockResolvedValue(mockNote22)

            const sagaTester = initTester()

            sagaTester.dispatch(sagaLoadNote(2))

            await sagaTester.waitFor(ACTION_SET_LOADING)

            const state = sagaTester.getState()
            expect(spy).toHaveBeenCalledWith(2)
            expect(selectNoteById(2)(state)).toEqual(mockNote22)
            expect(selectApiErrorShown(state)).toBeFalsy()
            expect(selectNotesLoading(state)).toBeFalsy()
        })

        it('should show error when exception is thrown', async () => {
            jest.spyOn(NotesAdapter, 'get').mockRejectedValue({})

            const sagaTester = initTester()

            sagaTester.dispatch(sagaLoadNote(2))

            await sagaTester.waitFor(ACTION_SET_LOADING)

            const state = sagaTester.getState()
            expect(sagaTester.wasCalled(ACTION_UPSERT_NOTE)).toBeFalsy()
            expect(selectApiErrorShown(state)).toBeTruthy()
            expect(selectNotesLoading(state)).toBeFalsy()
        })
    })
})
