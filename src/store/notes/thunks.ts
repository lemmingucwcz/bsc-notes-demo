import { Dispatch } from 'redux'
import { NoteMo } from '../../model/NoteMo'
// import { StoreState } from '../createInitialState'
// import { selectNextNoteId } from './selectors'
import { deleteNote, upsertNote } from './actions'
import { setApiErrorShown } from '../api/actions'
import NotesAdapter from '../../api/adapters/NotesAdapter'

/**
 * Promisified timeout
 *
 * @param ms Time to wait
 */
const delay = (ms: number) =>
    new Promise((resolve) => {
        window.setTimeout(resolve, ms)
    })

/**
 * API call helper - check whether to mock an error + handle mock error / error from API
 *
 * @param lambda Lamba that calls API
 * @param dispatch Redux dispatch
 * @param title When 'throw', mocks error
 */
const errorWrapper = async <T>(lambda: () => Promise<T>, dispatch: Dispatch, title?: string): Promise<T> => {
    try {
        // Small artificial wait to let UI effects show :-)
        await delay(500)

        if (title === 'throw') {
            // Simulate error
            throw new Error('Test error')
        }

        // Call the deal
        return lambda()
    } catch (e) {
        dispatch(setApiErrorShown(true))

        // Rethrow so that parent rejects
        throw e
    }
}

export const createNoteThunk = (title: string) => async (
    dispatch: Dispatch,
    // getState: () => StoreState,
): Promise<void> => {
    const newNote = await errorWrapper(() => NotesAdapter.create(title), dispatch, title)

    // If we create new note like that, you would be able to also insert notes
    // const newNote = { id: selectNextNoteId(getState()), title }

    dispatch(upsertNote(newNote))
}

export const updateNoteThunk = (updatedNote: NoteMo) => async (dispatch: Dispatch): Promise<void> => {
    await errorWrapper(() => NotesAdapter.update(updatedNote), dispatch, updatedNote.title)

    // We ignore note from server since it is always the same and use what it should return
    dispatch(upsertNote(updatedNote))
}

export const deleteNoteThunk = (noteId: number) => async (dispatch: Dispatch): Promise<void> => {
    await errorWrapper(() => NotesAdapter.delete(noteId), dispatch)
    dispatch(deleteNote(noteId))
}
