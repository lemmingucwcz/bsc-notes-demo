export const ROUTE_LIST = '/'
export const ROUTE_DETAIL = (noteId: number) => `/detail/${noteId}`
